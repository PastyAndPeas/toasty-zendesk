﻿#requires -version 2

<#
    .SYNOPSIS
    Toasty Zendesk

    .DESCRIPTION
          Connects to a hosted Zendesk Installation, retrieves and manipulates JSON data and provides Toast Notification Reminders

    .INPUTS
    C:\Toasty_Zendesk\curl\curl.exe.exe
    C:\Toasty_Zendesk\curl\curl.exe-ca-bundle.crt
    config.txt

    .NOTES
    Version:        0.2
    Author:         Connor Unsworth
    Creation Date:  29 Apr 2016
    Purpose/Change: Creates interactable Toast Notifications
  
#>



#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#Pre-Stage Module Imports                                                                                                                                                           #
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    [Windows.UI.Notifications.ToastNotificationManager, Windows.UI.Notifications, ContentType = WindowsRuntime] > $null
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    $sScriptVersion = "0.2"
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#Global Declerations                                                                                                                                                                #
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
    $tz = Get-WmiObject Win32_TimeZone 
    $count = 0
    $global:domain = $null
    $global:username = $null
    $global:password = $null
    $global:refreshTime = $null
$global:userID = $null
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#####################################################################################################################################################################################
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#List Of Commands
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#         create-toast                                                                                                                                                              #
#         create-xml                                                                                                                                                                #
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#####################################################################################################################################################################################
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


function create-Toast($xml)
    {
    <#
        .NAME
            Create Toast

        .DESCRIPTION
            Reads compiled XML file and generates useable Toast Notification

        .PARAMETER xml
            Necessary XML for Toast
            
        .EXAMPLE
            create-toast($xml)

        .NOTES
        -
    #>


        $toast = [Windows.UI.Notifications.ToastNotification]::new($xml)
        $toast.Tag = "Toasty Zendesk"
        $toast.Group = "Toasty Zendesk"
        $toast.ExpirationTime = [DateTimeOffset]::Now.AddMinutes(120)
        #$toast.SuppressPopup = $true
        $notifier = [Windows.UI.Notifications.ToastNotificationManager]::CreateToastNotifier("Toasty Zendesk")
        $notifier.Show($toast);
    }

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#####################################################################################################################################################################################
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function create-XML($subject,$recipient,$time)
    {
    <#
        .NAME
            Create-xml

        .DESCRIPTION
            Creates the XML code for the Windows Toast

        .PARAMETER subject
            Holds the subject of the ticket

        .PARAMETER Recipient
            Holds the latest submitted on the ticket

        .PARAMETER time
            Holds the time when the ticket was last updated

        .EXAMPLE
        create-xml("ticket issue","User","time")

        .NOTES
        -
    #>

        $template = [Windows.UI.Notifications.ToastNotificationManager]::GetTemplateContent([Windows.UI.Notifications.ToastTemplateType]::ToastText01)

        $template = @"
        <toast >
  
            <visual>
                <binding template="ToastGeneric">
                    <text>$subject</text>
                    <text>$recipient</text>
                    <text>$time</text>
                </binding>
            </visual>

            <actions>
    
                <input id="snoozeTime" type="selection" defaultInput="15">
                    <selection id="1" content="1 minute"/>
                    <selection id="15" content="15 minutes"/>
                    <selection id="60" content="1 hour"/>
                    <selection id="120" content="2 hours"/>
                </input>

            <action
                activationType="system"
                arguments="snooze"
                hint-inputId="snoozeTime"
                content=""/>

            <action
                activationType="system"
                arguments="dismiss"
                content=""/>
    
            </actions>
  
        </toast>
"@


    $xml = New-Object Windows.Data.Xml.Dom.XmlDocument
    $xml.LoadXml($template)
    return $xml
    }
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#####################################################################################################################################################################################
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function get-configData
{
<#
        .NAME
        get-configData

        .DESCRIPTION
        Reads in config data 

        .EXAMPLE
        get-configData

        .NOTES
        Imports Config File and holds all needed data
#>
        try{$configData = get-content "c:\Toasty_Zendesk\config.txt"}
        catch
            {
                #Error
                pause
            }
        
        foreach($line in $configData)
            {
                if($line -notmatch "#")
                    {
                        $data = $line.trim().split(";")
                        switch($data[0])
                        {
                            "Domain"{$global:domain = $data[1] + ".zendesk.com"}
                            
                            "Username"{$global:username = $data[1]}
                            
                            "Password"{$global:password = $data[1]}
                            
                            "RefreshTime"{$global:refreshTime = ($data[1]).replace('"','')}   
                        }
                    }
            }
        $url = 'C:\Toasty_Zendesk\curl\curl.exe "https://' + $global:domain +'/api/v2/search.json" -G --data-urlencode "query=type:user '+ ($global:username.split("@")[0]) + '" -v -u '+ $global:username + ':' + $global:password
        $global:userID = (@(cmd /c $url | convertfrom-json | ConvertTo-Json |  convertfrom-json).results).id
}
    
    
while($true)
{

#retrieve JSON data

    get-configData

    $url = 'C:\Toasty_Zendesk\curl\curl.exe "https://' + $global:domain +'/api/v2/search.json" -G --data-urlencode "query=type:ticket status:open assignee_id:'+  $global:userID + '" -v -u '+ $global:username + ':' + $global:password
    $openTicketsJSONData= @(cmd /c $url | convertfrom-json | ConvertTo-Json |  convertfrom-json) 

foreach($ticketData in $openTicketsJSONData.results)
    {
        $id = $ticketData.id.toString()
        $subject = $ticketData.subject.toString()
        $dateTime = @($ticketData.updated_at.toString().replace("Z","").split("T"))
        try{$type = $ticketData.type.toString()}
        catch{$type = "Issue"}

        $date  = [datetime](($ticketData.updated_at.toString().replace("Z","").split("T"))[0]).replace("-","/")+(($ticketData.updated_at.toString().replace("Z","").split("T"))[1])
        
        if (($date).IsDayLightSavingTime()) 
            { 
                $date=$date.AddHours(1)
            }
        
        if((get-date).AddMinutes(-10) -lt $date)
        {
            $count++
            write-host $subject
            $userData = $userData.name
            create-Toast(create-XML $subject $type $dateTime[0].toString())

        }

        if($count -gt 1)
        {
            create-Toast(create-XML "New Tickets" "You have recieved more than one reply!" (get-date -Format t))
        }

    }

    start-sleep -Seconds 600
}